function csvToJson(csvData) {
  let arrayOfObjects = [];

  // let [headerArray, ...entriesArray] = csvData;
  // headerArray = headerArray.split(",");
  // arrayOfObjects = entriesArray.map((csvEntry) => {
  //   return headerArray.reduce((result, entry, index) => {
  //     let entriesArray = separateValueArray(csvEntry);
  //     result[entry] = entriesArray[index]
  //     return result;
  //   },{});
  // });

  const headerArray = csvData[0].split(",");
  let seperatedArray = [];

  for (let i = 1; i < csvData.length; i++) {
    seperatedArray = separateValueArray(csvData[i]);
    arrayOfObjects.push(convertToObject(seperatedArray, headerArray));
  }

  return arrayOfObjects;
}

function convertToObject(seperatedArray, headerArray) {
  let lineObject = {};

  for (let i = 0; i < headerArray.length; i++) {
    lineObject[headerArray[i]] = seperatedArray[i];
  }
  return lineObject;
}

function separateValueArray(csvLineData) {
  let accumulator = "";
  seperatedArray = [];
  csvLineData += ",";

  for (let i = 0; i < csvLineData.length; i++) {
    accumulator += csvLineData[i];

    if (csvLineData[i] === '"') {
      i += 1;
      accumulator = "";
      while (csvLineData[i] !== '"') {
        accumulator += csvLineData[i];
        i++;
      }

      while (csvLineData[i] !== ",") {
        i++;
      }
      seperatedArray.push(accumulator);
      accumulator = "";
    } else if (csvLineData[i] === ",") {
      seperatedArray.push(accumulator.substring(0, accumulator.length - 1));
      accumulator = "";
    }
  }
  return seperatedArray;
}

module.exports = { csvToJson };