const fs = require("fs");

function writeOutput(JSONData, path) {
  if (!JSONData || !path) {
    throw new Error("wrong or no arguments passed");
  }

  let stringData;
  try {
    stringData = JSON.stringify(JSONData);
    fs.writeFileSync(path, stringData);
  } catch (e) {
    console.log(e.message);
  }
}

function matchIdsOfaYear(matchesData, year) {
  if (!Array.isArray(matchesData) || !Number.isInteger(year) || isNaN(year)) {
    throw new Error("enter valid arguments");
  }

  return matchesData
    .filter((matchDataElement) => {
      return parseInt(matchDataElement["season"]) === year ? true : false;
    })
    .map((element) => element["id"]);
}

function getBowlersInfo(deliveriesData, ids) {
  if (!Array.isArray(deliveriesData) || !Array.isArray(ids)) {
    throw new Error("enter valid arguments");
  }

  let result = {};

  result = deliveriesData.reduce((result, deliveriesDataElement) => {
    if (ids.includes(deliveriesDataElement["match_id"])) {
      let bowler = deliveriesDataElement["bowler"];
      let wide = parseInt(deliveriesDataElement["wide_runs"]);
      let noBall = parseInt(deliveriesDataElement["noball_runs"]);

      let runs =
        wide +
        parseInt(deliveriesDataElement["noball_runs"]) +
        parseInt(deliveriesDataElement["batsman_runs"]);

      if (result[bowler] === undefined) {
        result[bowler] = {};
        result[bowler]["runs"] = 0;
        result[bowler]["balls"] = 0;
      }

      if (wide || noBall) {
        result[bowler]["balls"] -= 1;
      }

      result[bowler]["runs"] += runs;
      result[bowler]["balls"] += 1;
    }

    return result;
  }, {});

  return result;
}

function readFile(path) {
  if (!fs.existsSync(path)) {
    throw new Error("file not found");
  }
  return fs.readFileSync(path, { encoding: "utf8" });
}

function makeJSONFile(JSONdata, path) {
  if (JSON === null || !isNaN(JSONdata) || !Array.isArray(JSONdata)) {
    throw new Error("invalid JSON");
  }
  let JSONString;
  try {
    JSONString = JSON.stringify(JSONdata);
  } catch (e) {
    console.log(e.message);
  }

  try {
    fs.writeFileSync(path, JSONString);
  } catch (e) {
    console.log(e.message);
  }
}

module.exports = {
  writeOutput,
  matchIdsOfaYear,
  getBowlersInfo,
  readFile,
  makeJSONFile,
};
