const { matchesPlayedPerYear } = require("./questions/matchesPerYear");
const { matchesWonPerTeam } = require("./questions/matchesWonPerTeam");
const { runsConcededPerTeam } = require("./questions/runsConcededPerTeam");
const { topEconomicalBowlers } = require("./questions/topBowlers");
const {getJSONData} = require("./getJSON");

let [matchesJSON, deliveriesJSON] = getJSONData();

//************** calling functions for respective questions ****************/

try {
  matchesPlayedPerYear(matchesJSON);
} catch (e) {
  console.log(e.message);
}

try {
  matchesWonPerTeam(matchesJSON);
} catch (e) {
  console.log(e);
}

try {
  runsConcededPerTeam(deliveriesJSON, matchesJSON, 2016);
} catch (e) {
  console.log(e);
}

try {
  topEconomicalBowlers(deliveriesJSON, matchesJSON, 2015);
} catch (e) {
  console.log(e);
}
