const { CONSTANTS } = require("./constants");
const fs = require("fs");
const { makeJSONFile, readFile } = require("./helperFunctions.js");
const { csvToJson } = require("./../data/csvToJSON");

function getJSONData() {
  // *****************************initialization******************************//
  // *********************** generating json from csv ************************//
  // *************** storing json data in global variables *****************//

  let deliveriesJSON;
  let matchesJSON;

  if (
    fs.existsSync(CONSTANTS.DELIVERIES_JSON_FILE_PATH) &&
    fs.existsSync(CONSTANTS.MATCHES_JSON_FILE_PATH)
  ) {
    let stringDeliveriesJSON = readFile(CONSTANTS.DELIVERIES_JSON_FILE_PATH);
    let stringMatchesJSON = readFile(CONSTANTS.MATCHES_JSON_FILE_PATH);

    try {
      deliveriesJSON = JSON.parse(stringDeliveriesJSON);
    } catch (e) {
      console.log(e.message);
    }

    try {
      matchesJSON = JSON.parse(stringMatchesJSON);
    } catch (e) {
      console.log(e.message);
    }
  } else {
    console.log("JSON files not found: trying generating JSON from csv files");

    if (
      !fs.existsSync(CONSTANTS.DELIVERIES_CSV_FILE_PATH) &&
      !fs.existsSync(CONSTANTS.MATCHES_CSV_FILE_PATH)
    ) {
      console.log("CSV files not found");
      process.exit();
      // throw new Error("csv files not found");
    } else {
      let deliveriesCSV = readFile(CONSTANTS.DELIVERIES_CSV_FILE_PATH).split("\r\n");
      let matchesCSV = readFile(CONSTANTS.MATCHES_CSV_FILE_PATH).split("\r\n");

      deliveriesJSON = csvToJson(deliveriesCSV);
      matchesJSON = csvToJson(matchesCSV);

      try {
        makeJSONFile(matchesJSON, CONSTANTS.MATCHES_JSON_FILE_PATH);
      } catch (e) {
        console.log(e.message);
      }

      try {
        makeJSONFile(deliveriesJSON, CONSTANTS.DELIVERIES_JSON_FILE_PATH);
      } catch (e) {
        console.log(e.message);
      }
    }
  }

  return [matchesJSON, deliveriesJSON];
}

module.exports = { getJSONData };
