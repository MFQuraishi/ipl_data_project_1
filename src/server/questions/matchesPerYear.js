const { writeOutput } = require("./../helperFunctions");
const { CONSTANTS } = require("./../constants");

function matchesPlayedPerYear(matchesData) {
  if (!Array.isArray(matchesData)) {
    throw new Error("enter valid argument");
  }

  let matchesPlayedPerYear = {};

  matchesPlayedPerYear = matchesData.reduce((matchesPlayedPerYear, matchDataElement) => {
    let { season } = matchDataElement;
    if (matchesPlayedPerYear[season] !== undefined) {
      matchesPlayedPerYear[season] += 1;
    } else {
      matchesPlayedPerYear[season] = 1;
    }
    return matchesPlayedPerYear;
  }, {});

  try {
    writeOutput(matchesPlayedPerYear, CONSTANTS.MATCHES_PER_YEAR_FILE_PATH);
  } catch (e) {
    console.log(e);
  }
  return matchesPlayedPerYear;
}

module.exports = { matchesPlayedPerYear };
