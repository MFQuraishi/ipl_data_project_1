const { writeOutput, matchIdsOfaYear } = require("./../helperFunctions");
const { CONSTANTS } = require("./../constants");

function runsConcededPerTeam(deliveriesData, matchesData, year) {
  if (
    !Array.isArray(deliveriesData) ||
    !Array.isArray(matchesData) ||
    typeof year !== "number" ||
    isNaN(year)
  ) {
    throw new Error("enter valid arguments");
  }

  let ids = matchIdsOfaYear(matchesData, year);

  let result = {};

  result = deliveriesData.reduce((result, deliveriesDataElement) => {
    if (ids.includes(deliveriesDataElement["match_id"])) {
      let {bowling_team: bowlingTeam, extra_runs: extraRuns} = deliveriesDataElement;

      if (result[bowlingTeam] === undefined) {
        result[bowlingTeam] = parseInt(extraRuns);
      } else {
        result[bowlingTeam] += parseInt(extraRuns);
      }
    }
    return result;
  }, {});

  try {
    writeOutput(result, CONSTANTS.RUNS_CONCEDED_PER_TEAM_FILE_PATH);
  } catch (e) {
    console.log(e);
  }
  return result;
}

module.exports = { runsConcededPerTeam };
