const { writeOutput, matchIdsOfaYear, getBowlersInfo } = require("./../helperFunctions");
const { CONSTANTS } = require("./../constants");

function topEconomicalBowlers(deliveriesData, matchesData, year) {
  if (
    typeof matchesData !== "object" ||
    typeof matchesData !== "object" ||
    typeof year !== "number" ||
    isNaN(year)
  ) {
    throw new Error("enter valid arguments");
  }

  let ids = matchIdsOfaYear(matchesData, year);
  let bowlersInfo = getBowlersInfo(deliveriesData, ids);

  // bowlerInfo = {"BOWLER_NAME": {balls: BALLS, runs: RUNS}, ....}
  let economies = Object.entries(bowlersInfo).map((bowlerInfoElement) => {
    overs = bowlerInfoElement[1]["balls"] / 6;
    runs = bowlerInfoElement[1]["runs"];
    return [bowlerInfoElement[0], runs / overs];
  });

  economies.sort((a, b) => a[1] - b[1]);

  let result = economies.slice(0, 10).map((info) => {
    return info[0];
  });

  try {
    writeOutput(result, CONSTANTS.TOP_BOWLERS_FILE_PATH);
  } catch (e) {
    console.log(e);
  }

  return result;
}

module.exports = { topEconomicalBowlers };
