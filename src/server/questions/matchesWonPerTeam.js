const { writeOutput } = require("./../helperFunctions");
const { CONSTANTS } = require("./../constants");

function matchesWonPerTeam(matchesData) {
  if (!Array.isArray(matchesData)) {
    throw new Error("enter valid arguments");
  }

  wonPerTeam = {};

  wonPerTeam = matchesData.reduce((wonPerTeam, matchDataElement) => {
    let { season, winner } = matchDataElement;

    if (typeof wonPerTeam[season] === "undefined") {
      wonPerTeam[season] = {};
    }
    if (wonPerTeam[season][winner] !== undefined) {
      wonPerTeam[season][winner] += 1;
    } else {
      wonPerTeam[season][winner] = 1;
    }

    return wonPerTeam;
  }, {});

  try {
    writeOutput(wonPerTeam, CONSTANTS.MATCHES_PER_TEAM_PER_YEAR_FILE_PATH);
  } catch (e) {
    console.log(e);
  }
  return wonPerTeam;
}

module.exports = { matchesWonPerTeam };
