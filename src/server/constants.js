const CONSTANTS = {
  MATCHES_CSV_FILE_PATH: "./../data/matches.csv",
  DELIVERIES_CSV_FILE_PATH: "./../data/deliveries.csv",
  MATCHES_JSON_FILE_PATH: "./../data/matches.json",
  DELIVERIES_JSON_FILE_PATH: "./../data/deliveries.json",

  MATCHES_PER_YEAR_FILE_PATH: "./../public/output/matchesPerYear.json",
  MATCHES_PER_TEAM_PER_YEAR_FILE_PATH: "./../public/output/matchesWonPerTeam.json",
  RUNS_CONCEDED_PER_TEAM_FILE_PATH: "./../public/output/RunsConcededPerTeam.json",
  TOP_BOWLERS_FILE_PATH: "./../public/output/topBowlers.json",
};

module.exports = { CONSTANTS };
