const { runsConcededPerTeam } = require("./../server/questions/runsConcededPerTeam");
const { dummyMatches, dummyDeliveries } = require("./dummyData.js");

describe("testing for ipl question 3 (runs conceded per team)", () => {
  test("test for valid inputs", () => {
    expect(runsConcededPerTeam(dummyDeliveries, dummyMatches, 2017)).toEqual({
      "Royal Challengers Bangalore": 4,
    });
  });

  test("for falsy values", () => {
    expect(() => runsConcededPerTeam(null, null, null)).toThrow();
  });

  test("for empty strings", () => {
    expect(() => runsConcededPerTeam("", "", "")).toThrow();
  });

  test("for undefined", () => {
    expect(() => runsConcededPerTeam(undefined, undefined, undefined)).toThrow();
  });
  test("for false", () => {
    expect(() => runsConcededPerTeam(false, false, false)).toThrow();
  });

  test("for 0", () => {
    expect(() => runsConcededPerTeam(0, 0, 0)).toThrow();
  });

  test("for NaN", () => {
    expect(() => runsConcededPerTeam(NaN, NaN, NaN)).toThrow();
  });

  test("for mixed values", () => {
    expect(() => runsConcededPerTeam(dummyDeliveries, dummyMatches, NaN)).toThrow();
    expect(() => runsConcededPerTeam(dummyDeliveries, NaN, 2017)).toThrow();
    expect(() => runsConcededPerTeam(NaN, dummyMatches, 2017)).toThrow();
  });

  test("for empty arrays", () => {
    expect(runsConcededPerTeam([], dummyMatches, 2017)).toEqual({});
    expect(runsConcededPerTeam(dummyDeliveries, [], 2017)).toEqual({});
    expect(runsConcededPerTeam([], [], 2017)).toEqual({});
  });

  test("for empty objects", () => {
    expect(() => runsConcededPerTeam({}, dummyMatches, 2017)).toThrow();
    expect(() => runsConcededPerTeam(dummyDeliveries, {}, 2017)).toThrow();
    expect(() => runsConcededPerTeam({}, {}, 2017)).toThrow();
  });
  
});
