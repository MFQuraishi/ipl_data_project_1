const { topEconomicalBowlers } = require("./../server/questions/topBowlers");
const { dummyMatches, dummyDeliveries } = require("./dummyData.js");

describe("testing for ipl question 4 (top economical bowlers)", () => {
  test("test for valid inputs", () => {
    expect(topEconomicalBowlers(dummyDeliveries, dummyMatches, 2017)).toEqual([
      "TS Mills",
    ]);
  });

  test("for falsy values", () => {
    expect(() => topEconomicalBowlers(null, null, null)).toThrow();
  });

  test("for empty string", () => {
    expect(() => topEconomicalBowlers("", "", "")).toThrow();
  });

  test("for undefined", () => {
    expect(() => topEconomicalBowlers(undefined, undefined, undefined)).toThrow();
  });

  test("for false", () => {
    expect(() => topEconomicalBowlers(false, false, false)).toThrow();
  });

  test("for 0", () => {
    expect(() => topEconomicalBowlers(0, 0, 0)).toThrow();
  });

  test("for NaN", () => {
    expect(() => topEconomicalBowlers(NaN, NaN, NaN)).toThrow();
  });

  test("for mixed values", () => {
    expect(() => topEconomicalBowlers(dummyDeliveries, dummyMatches, NaN)).toThrow();
    expect(() => topEconomicalBowlers(dummyDeliveries, NaN, 2017)).toThrow();
    expect(() => topEconomicalBowlers(NaN, dummyMatches, 2017)).toThrow();
  });

  test("for empty arrays", () => {
    expect(topEconomicalBowlers([], dummyMatches, 2017)).toEqual([]);
    expect(topEconomicalBowlers(dummyDeliveries, [], 2017)).toEqual([]);
    expect(topEconomicalBowlers([], [], 2017)).toEqual([]);
  });

  test("for empty objects", () => {
    expect(() => topEconomicalBowlers({}, dummyMatches, 2017)).toThrow();
    expect(() => topEconomicalBowlers(dummyDeliveries, {}, 2017)).toThrow();
    expect(() => topEconomicalBowlers({}, {}, 2017)).toThrow();
  });
});
