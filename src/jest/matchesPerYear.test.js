const { matchesPlayedPerYear } = require("./../server/questions/matchesPerYear");
const { dummyMatches } = require("./dummyData.js");

describe("testing for ipl question 1 (matches played per year)", () => {
  test("for valid values", () => {
    expect(matchesPlayedPerYear(dummyMatches)).toEqual({ 2017: 3 });
  });

  test("for falsy values", () => {
    expect(() => matchesPlayedPerYear(null)).toThrow();
  });
  test("for empty string", () => {
    expect(() => matchesPlayedPerYear("")).toThrow();
  });
  test("for false", () => {
    expect(() => matchesPlayedPerYear(false)).toThrow();
  });

  test("for undefined", () => {
    expect(() => matchesPlayedPerYear(undefined)).toThrow();
  });

  test("for 0", () => {
    expect(() => matchesPlayedPerYear(0)).toThrow();
  });

  test("for NaN", () => {
    expect(() => matchesPlayedPerYear(NaN)).toThrow();
  });

  test("for empty array", () => {
    expect(matchesPlayedPerYear([])).toEqual({});
  });

  test("for empty object", () => {
    expect(() => matchesPlayedPerYear({})).toThrow();
  });
});
