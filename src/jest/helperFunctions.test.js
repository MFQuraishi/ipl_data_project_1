const { matchIdsOfaYear, getBowlersInfo } = require("./../server/helperFunctions");
const { dummyMatches, dummyDeliveries } = require("./dummyData.js");

describe("testing for match Ids of a year function", () => {
  test("test for valid inputs", () => {
    expect(matchIdsOfaYear(dummyMatches, 2017)).toEqual(["1", "2", "3"]);
  });

  test("for null", () => {
    expect(() => matchIdsOfaYear(null, null)).toThrow();
  });

  test("for empty string", () => {
    expect(() => matchIdsOfaYear("", "")).toThrow();
  });

  test("for undefined", () => {
    expect(() => matchIdsOfaYear(undefined, undefined)).toThrow();
  });

  test("for false", () => {
    expect(() => matchIdsOfaYear(false, false)).toThrow();
  });

  test("for 0", () => {
    expect(() => matchIdsOfaYear(0, 0)).toThrow();
  });

  test("for NaN", () => {
    expect(() => matchIdsOfaYear(NaN, NaN)).toThrow();
  });

  test("for mixed values", () => {
    expect(() => matchIdsOfaYear(dummyMatches, NaN)).toThrow();
    expect(() => matchIdsOfaYear(NaN, 2017)).toThrow();
  });

  test("for empty arrays", () => {
    expect(matchIdsOfaYear([], 2017)).toEqual([]);
  });

  test("for empty objects", () => {
    expect(() => matchIdsOfaYear({}, 2017)).toThrow();
  });
});

describe("testing for getBowlerInfo function", () => {
  let ids = matchIdsOfaYear(dummyMatches, 2017);

  test("test for valid inputs", () => {
    expect(getBowlersInfo(dummyDeliveries, ids)).toEqual({
      "TS Mills": {
        runs: 0,
        balls: 2,
      },
    });
  });

  test("for falsy values", () => {
    expect(() => getBowlersInfo(null, null)).toThrow();
  });
  test("for empty string", () => {
    expect(() => getBowlersInfo("", "")).toThrow();
  });
  test("for null", () => {
    expect(() => getBowlersInfo(undefined, undefined)).toThrow();
  });

  test("for false", () => {
    expect(() => getBowlersInfo(false, false)).toThrow();
  });
  test("for 0", () => {
    expect(() => getBowlersInfo(0, 0)).toThrow();
  });

  test("for NaN", () => {
    expect(() => getBowlersInfo(NaN, NaN)).toThrow();
  });

  test("for mixed values", () => {
    expect(() => getBowlersInfo(dummyDeliveries, NaN)).toThrow();
    expect(() => getBowlersInfo(NaN, ids)).toThrow();
  });
    
  test("for empty arrays", () => {
    expect(getBowlersInfo([], ids)).toEqual({});
  });

  test("for empty objects", () => {
    expect(() => getBowlersInfo({}, ids)).toThrow();
  });
});
