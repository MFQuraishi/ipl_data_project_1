const { matchesWonPerTeam } = require("./../server/questions/matchesWonPerTeam");
const { dummyMatches } = require("./dummyData.js");

describe("testing for ipl question 2 (matches won per team per year)", () => {
  test("for valid values", () => {
    expect(matchesWonPerTeam(dummyMatches)).toEqual({
      2017: {
        "Kolkata Knight Riders": 1,
        "Rising Pune Supergiant": 1,
        "Sunrisers Hyderabad": 1,
      },
    });
  });

  test("for null value", () => {
    expect(() => matchesWonPerTeam(null)).toThrow();
  });

  test("for empty string", () => {
    expect(() => matchesWonPerTeam("")).toThrow();
  });

  test("for undefined", () => {
    expect(() => matchesWonPerTeam(undefined)).toThrow();
  });

  test("for false", () => {
    expect(() => matchesWonPerTeam(false)).toThrow();
  });

  test("for 0", () => {
    expect(() => matchesWonPerTeam(0)).toThrow();
  });

  test("for NaN", () => {
    expect(() => matchesWonPerTeam(NaN)).toThrow();
  });

  test("for empty array", () => {
    expect(matchesWonPerTeam([])).toEqual({});
  });
  
  test("for empty object", () => {
    expect(() => matchesWonPerTeam({})).toThrow();
  });
});
